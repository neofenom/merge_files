<?php
error_reporting(E_ALL);
ini_set("display_errors", "On");

$filesToMerge = array();
$mergeResultText = '';
$config = file("merge_config");

init();
$filesToMerge = loadFileNamesFromConfig($config);

if ($argc < 2) {
    die ('Error: please specify result file as script parameter');
}

if ($mergeResultFile = $argv[1]) {
    $mergedCount = mergeFiles($filesToMerge, $mergeResultFile);
    echo " $mergedCount files are merged to $mergeResultFile.\n";
}

exit(0);


function init() {
    if (PHP_SAPI != "cli") {
        exit;
    }
}

function loadFileNamesFromConfig($config){
    if (!$config) {
        die("Config 'merge_config' missing in script directory!");
    }

    $filesToMerge = array();
    if (is_array($config)) {
        foreach ($config as $pathLine) {

            $filePath = trim($pathLine);
            $pathOptions = explode(',', $filePath);
            $filePath = $pathOptions[0];
            if (isset($pathOptions[1])) {
                $fileToMerge['replaceFrom'] = $pathOptions[1];
            }
            if (isset($pathOptions[2])) {
                $fileToMerge['replaceTo'] = $pathOptions[2];
            }

            if (file_exists($filePath)) {
                $fileToMerge['fileName'] = $filePath;
                $filesToMerge[] = $fileToMerge;
            } else {
                echo "\nWarning: file '$filePath' not found'\n";
            }
        }

    }
    return $filesToMerge;
}

/**
 * @param $srcFileList array
 * @param $resultFileName string
 * @return int
 */
function mergeFiles($srcFileList, $resultFileName){
    $resultFile = fopen($resultFileName, 'w');
    if (!$resultFile) {
        die ("Error: cannot open result file '$resultFileName' for writing");
    }
    $mergedCount = 0;
    foreach ($srcFileList as $fileData) {
        if ($file = file($fileData['fileName'])) {
        
            foreach ($file as $line) {
                if (isset($fileData['replaceFrom']) && isset($fileData['replaceTo'])){
                    $line = str_replace($fileData['replaceFrom'], $fileData['replaceTo'], $line);
                }
                fputs($resultFile, $line);
            }
           
            ++$mergedCount;
        } else {
            echo "\nWarning: cannot load file '{$fileData['fileName']}'\n";
        }
    }
    fputs($resultFile,  "\n");
    return $mergedCount;

}

