# README #

Simple script for merging text files into one

### Configuration ###

Files for merging need to be specified in config file `merge_config` , 
Optionally replace string can be specified after filename

```
file1.txt
file2.txt,<replaceStringFrom>,<replaceStringTo>
...
```

### Usage ###

php merge_files.php result_file.txt